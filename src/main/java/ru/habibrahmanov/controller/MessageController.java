package ru.habibrahmanov.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;
import ru.habibrahmanov.model.Message;
import ru.habibrahmanov.service.MessageService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("message")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public List<Message> getList() {
        return messageService.findAll();
    }

    @GetMapping("{id}")
    public Message getOne(@PathVariable("id") @NotNull final Message message) {
        return message;
    }

    @PostMapping
    public Message create(@RequestBody @NotNull final Message message) {
        message.setLocalDateTime(LocalDateTime.now());
        return messageService.save(message);
    }

    @PutMapping("{id}")
    public Message update(
            @PathVariable("id") @NotNull final Message messageFromDb,
            @RequestBody @NotNull final Message message
    ) {
        BeanUtils.copyProperties(message, messageFromDb, "id");
        return messageService.save(messageFromDb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") @NotNull final Message message) {
        messageService.delete(message);
    }

    @MessageMapping("/changeMessage")
    @SendTo("/topic/activity")
    public Message change(@NotNull final Message message) {
        return messageService.save(message);
    }
}