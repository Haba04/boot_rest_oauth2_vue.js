package ru.habibrahmanov.controller;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.model.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.habibrahmanov.service.MessageService;

import java.util.HashMap;

@Controller
@RequestMapping("/")
public class MainController {
    private final MessageService messageService;

    public MainController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public String main(@NotNull final Model model, @AuthenticationPrincipal @NotNull final User user) {
        @NotNull final HashMap<Object, Object> data = new HashMap<>();
        data.put("profile", user);
        data.put("messages", messageService.findAll());
        model.addAttribute("frontendData", data);
        return "index";
    }
}