package ru.habibrahmanov.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.habibrahmanov.model.Message;
import ru.habibrahmanov.repository.MessageRepository;

import java.util.List;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Nullable
    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    @Nullable
    public Message save(@Nullable final Message message) {
        if (message == null) throw new IllegalArgumentException();
        return messageRepository.save(message);
    }

    public void delete(@Nullable final Message message) {
        if (message == null) throw new IllegalArgumentException();
        messageRepository.delete(message);
    }
}
