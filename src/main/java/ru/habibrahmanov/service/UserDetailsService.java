package ru.habibrahmanov.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.habibrahmanov.model.User;
import ru.habibrahmanov.repository.UserDetailsRepository;

import java.util.List;

@Service
public class UserDetailsService {
    private final UserDetailsRepository userDetailsRepository;

    public UserDetailsService(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @Nullable
    public List<User> findAll() {
        return userDetailsRepository.findAll();
    }
}
