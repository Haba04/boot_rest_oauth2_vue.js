package ru.habibrahmanov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.model.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
}