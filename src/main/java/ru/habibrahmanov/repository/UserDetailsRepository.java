package ru.habibrahmanov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.habibrahmanov.model.User;

@Repository
public interface UserDetailsRepository extends JpaRepository<User, String> {
}
